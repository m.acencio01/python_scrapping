import requests, json
from bs4 import BeautifulSoup

source = ""

# Extraer la fuente de la página de lista de servicios
source = requests.get('https://www.temucoteofrece.com/providers/list').text

# Si no está disponible, o para evitar un bloqueo por sobreuso, utilice el html fuera de línea
# with open("test.html") as f:
#     source = f.read()

# Parsear la fuente a un soup
soup = BeautifulSoup(source, 'lxml')

# Recortarle el pelo un poco
soup.prettify()

# Extraer la sección con todos los servicios
full_box = soup.find_all("section", attrs={'class': 'box'})[-1]

# Extraer cada cuadro con el servicio
figures = full_box.find_all('figure')

# Formatear la información de cada servicio a una lista
service_list = []

for fig in figures:
    cat_dir = fig.find_all('small')
    url = "https://www.temucoteofrece.com" + fig.a['href']
    service = {'nombre': fig.h3.text,
               'categoria': cat_dir[0].text,
               'direccion': cat_dir[1].text,
               'imagen': fig.img['src'],
               'url': url}
    service_list.append(service)

# Convertir la lista de servicios a json
print(json.dumps(service_list))
